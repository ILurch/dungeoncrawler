﻿using GXPEngine.Core;

namespace Game
{
    public struct Hitbox
    {

        private float _lRmargin;
        private float _tDmargin;
        
        public Hitbox(float lRmargin, float tdMargin)
        {
            _lRmargin = lRmargin;
            _tDmargin = tdMargin;
        }

        public float GetLeftRightMargin()
        {
            return _lRmargin;
        }

        public float GetTopDownMargin()
        {
            return _tDmargin;
        }

    }
}