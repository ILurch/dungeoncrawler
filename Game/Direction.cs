﻿namespace Game
{
    public enum Direction
    {
        NORTH,
        EAST,
        SOUTH,
        WEST
    }
}