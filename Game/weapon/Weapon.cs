﻿using System;
using Game.entity;
using GXPEngine;
using GXPEngine.Core;

namespace Game
{
    public class Weapon : GameObject
    {
        private Creature owner;

        private bool shooting;

        private const float speed = 10f;
        private const float bulletSpeed = 12f;
        private int count;
        
        private static readonly Sound _shoot = new Sound("assets/audio/shoot.mp3");

        public Weapon(Creature owner, int MaxClipSize)
        {
            this.owner = owner;
            this.MaxClipSize = MaxClipSize;
            CurrentAmmo = MaxClipSize;
        }

        public int MaxClipSize { get; set; }
        public int CurrentAmmo { get; set; }


        public void holdTrigger()
        {
            shooting = true;
        }

        public void releaseTrigger()
        {
            shooting = false;
        }

        private void Shoot()
        {
            Vector2 v = Utils.GetDirectionAsVector(owner.GetDirection());
            v.Multiply(bulletSpeed);
            Bullet bullet = new Bullet(EntityType.Bullet, owner, v);
            MyGame.Instance().GetCurrentRoom().AddEntity(bullet);
            CurrentAmmo--;
            count = 0;
            _shoot.Play();
        }

        public void Update()
        {
            if (count > speed && CurrentAmmo > 0)
            {
                if (shooting)
                    Shoot();
            }
            else
                count++;
        }
    }
}