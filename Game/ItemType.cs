﻿namespace Game
{
    public enum ItemType
    {
        Bullet,
        Trap,
        Health
    }
}