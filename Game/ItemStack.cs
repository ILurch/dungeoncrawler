﻿namespace Game
{
    // Represents all items on a stack
    public class ItemStack
    {
        private ItemType _itemType;
        private string _name;

        public ItemStack(ItemType itemType, int amount, string name)
        {
            _itemType = itemType;
            Amount = amount;
            _name = name;
        }

        public int Amount { get; set; }

        public ItemType GetMaterial()
        {
            return _itemType;
        }

        public string GetName()
        {
            return _name;
        }
    }
}