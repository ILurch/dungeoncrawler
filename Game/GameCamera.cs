﻿using System;
using GXPEngine;

namespace Game
{
    public class GameCamera : GameObject
    {
        private new readonly MyGame game;

        public GameCamera(MyGame game)
        {
            this.game = game;
        }

        public void Update()
        {
            // Center the camera to the player
            x = -1 * game.GetPlayer().GetPosition().x + game.width / 2f + game.GetPlayer().width / 2f;
            y = -1 * game.GetPlayer().GetPosition().y + game.height / 2f + game.GetPlayer().height / 2f;

            /* Limit the camera to the rooms width and height
            if (y > 0)
                y = 0;
            if (x > 0)
                x = 0;
            if (x < -1 * (game.GetCurrentRoom().GetWidth() * Utils.materials.Width - game.width))
                x = -1 * (game.GetCurrentRoom().GetWidth() * Utils.materials.Width - game.width);
            if (y < -1 * (game.GetCurrentRoom().GetHeight() * Utils.materials.Height - game.height))
                y = -1 * (game.GetCurrentRoom().GetHeight() * Utils.materials.Height - game.height);
            */
        }
    }
}