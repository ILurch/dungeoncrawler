﻿namespace Game.Map
{
    public enum Material
    {
        AIR=133,
        FLOOR_1 = 0,
        FLOOR_2 = 1,
        FLOOR_3 = 2,
        FLOOR_4 = 3,
        FLOOR_5 = 17,
        FLOOR_6 = 18,
        FLOOR_7 = 19,
        FLOOR_8 = 20,
        WALL_1 = 4,
        WALL_2 = 5,
        WALL_3 = 6,
        WALL_4 = 7,
        WALL_5 = 8,
        WALL_6 = 9,
        WALL_7 = 10,
        WALL_8 = 11,
        WALL_9 = 12,
        WALL_10 = 13,
        WALL_11 = 14,
        WALL_12 = 15,
        WALL_13 = 16,
        
        DOOR_BOTTOM=128,
        DOOR_TOP = 111,
        
    }
}