﻿using System.Collections.Generic;
using Game.entity;
using GXPEngine;
using GXPEngine.Core;

namespace Game.Map
{
    public class Room : GameObject
    {
        private int _pos;
        private Tile[,] _tiles;
        private List<Door> _doors;
        private RoomData _data;

        public void UpdateFrom(int id, RoomData data)
        {
            _data = data;
            _pos = id;
            _doors = data.GetDoors();
            _tiles = new Tile[data.GetDataArr().GetLength(0), data.GetDataArr().GetLength(1)];
            for (var i = 0; i < _tiles.GetLength(0); i++)
            {
                for (var j = 0; j < _tiles.GetLength(1); j++)
                {
                    Material material;
                    if (data.GetDataArr()[i, j] == -1)
                        material = Material.AIR;
                    else
                        material = (Material) data.GetDataArr()[i, j];

                    var solid = material.ToString().StartsWith("WALL") || material.ToString().StartsWith("DOOR");
                    var tile = new Tile(material, solid);
                    _tiles[i, j] = tile;
                    tile.SetXY(i * tile.width, j * tile.height);
                    AddChild(tile);
                }
            }
            foreach (Vector2 v in data.GetTraps())
            {
                Trap trap = new Trap(v.x, v.y, 0, 0);
                AddEntity(trap);
            }
            foreach (var keyData in data.GetKeys())
            {
                var key = new entity.Key(keyData);
                AddEntity(key);
            }
            foreach (var chestData in data.GetChests())
            {
                var chest = new Chest(chestData);
                AddEntity(chest);
            }
            foreach (Zombie.ZombieData zdata in data.GetZombies())
            {
                Zombie zomb = new Zombie(zdata);
                AddEntity(zomb);
            }
        }

        public RoomData GetRoomData()
        {
            return _data;
        }

        public List<Door> GetDoors()
        {
            return _doors;
        }

        public void AddEntity(Entity ent)
        {
            AddChild(ent);
        }

        public int GetId()
        {
            return _pos;
        }

        public void SetId(int id)
        {
            _pos = id;
        }

        public Tile[,] GetTiles()
        {
            return _tiles;
        }

        /// <summary>
        /// Parameters are the indexes of the tile array
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public Tile GetTile(int x, int y)
        {
            if (x >= 0 && x < GetWidth() && y >= 0 && y < GetHeight())
            {
                return _tiles[x, y];
            }
            return null;
        }

        /// <summary>
        /// Parameters are relative positions in the room
        /// </summary>
        /// <param name="xF"></param>
        /// <param name="yF"></param>
        /// <returns></returns>
        public Tile GetTileRel(float xF, float yF)
        {
            int x = (int) (xF /= 32);
            int y = (int) (yF /= 32);

            if (x >= 0 && x < GetWidth() && y >= 0 && y < GetHeight())
            {
                return _tiles[x, y];
            }
            return null;
        }
        
        public int GetWidth()
        {
            return _tiles.GetLength(0);
        }

        public int GetHeight()
        {
            return _tiles.GetLength(1);
        }
    }
}