﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using Game.entity;
using GXPEngine.Core;

namespace Game.Map
{
    public struct RoomData
    {
        private int[,] _dataArr;
        private Vector2 _playerSpawn;
        private List<Door> _doors;
        private List<Zombie.ZombieData> _zombies;
        private List<Vector2> _traps;
        private List<Key.KeyData> _keys;
        private List<Chest.ChestData> _chests;
        
        public RoomData(int[,] dataArr, Vector2 playerSpawn, List<Door> doors, List<Zombie.ZombieData> zombies, List<Vector2> traps, List<Key.KeyData> keys, List<Chest.ChestData> chests)
        {
            _dataArr = dataArr;
            _playerSpawn = playerSpawn;
            _doors = doors;
            _zombies = zombies;
            _traps = traps;
            _keys = keys;
            _chests = chests;
        }

        public int[,] GetDataArr()
        {
            return _dataArr;
        }

        public List<Door> GetDoors()
        {
            return _doors;
        }
        
        public Vector2 GetPlayerSpawn()
        {
            return _playerSpawn;
        }

        public void SetPlayerSpawn(float f, float f1)
        {
            _playerSpawn.x = f;
            _playerSpawn.y = f1;
        }

        public List<Zombie.ZombieData> GetZombies()
        {
            return _zombies;
        }

        public List<Vector2> GetTraps()
        {
            return _traps;
        }

        public List<Key.KeyData> GetKeys()
        {
            return _keys;
        }

        public List<Chest.ChestData> GetChests()
        {
            return _chests;
        }
    }
}