﻿using System.Drawing;
using GXPEngine;
using GXPEngine.Core;

namespace Game.Map
{
    public class Tile : AnimationSprite
    {
        public static readonly Bitmap TileSetImage = (Bitmap) Image.FromFile("assets/tileset.png");

        private readonly Material _material;
        private bool solid;

        public Tile(Material material, bool solid) : base(TileSetImage, 17, 9)
        {
            this._material = material;
            this.solid = solid;
            SetFrame((int) material);
        }

        public Material GetMaterial()
        {
            return _material;
        }

        public bool IsSolid()
        {
            return solid;
        }
    }
}