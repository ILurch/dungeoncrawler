﻿using System.Runtime.Remoting.Metadata;
using GXPEngine.Core;

namespace Game.Map
{
    public class Door
    {
        private readonly int _destination;
        private Vector2 pos;

        public Door(int destination, float x, float y)
        {
            pos = new Vector2(x, y);
            _destination = destination;
            Locked = true;
        }

        public int GetDestination()
        {
            return _destination;
        }

        public bool Locked { get; set; }

        public Vector2 GetPosition()
        {
            return pos;
        }
        
    }
}