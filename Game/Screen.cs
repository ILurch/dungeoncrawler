﻿using System;
using System.Drawing;
using GXPEngine;

namespace Game
{
    public class Screen : Canvas
    {
        private Font _font;
        private string _title;
        private SolidBrush _brush;

        public Screen(string title, int width, int height) : base(width, height)
        {
            _font = new Font("assets/fonts/arial.ttf", 16, FontStyle.Regular);
            _title = title;
            _brush = new SolidBrush(Color.BurlyWood);
        }

        public void Update()
        {
            int titleLength = (int) _font.Size * _title.Length;
            graphics.DrawString(_title, _font, _brush, new PointF(width / 2f - (titleLength / 3f), height / 2f));
        }

        public string GetTitle()
        {
            return _title;
        }
    }
}