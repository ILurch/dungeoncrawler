﻿using System;
using Game.Map;
using GXPEngine;

namespace Game.entity
{
    public class Trap : Entity
    {
        private int _count;
        private  const int _triggerTime = 100;
        private int _triggerCount;
        private AnimationSprite _sprite;
        private AnimationSprite _fire;
        private EntityState _fireState = new EntityState(0, 3);

        public Trap(float x, float y, float hitboxLR, float hitboxTD) : base(EntityType.Trap, x, y, hitboxLR, hitboxTD,
            false, false)
        {
            // remove shadow
            alpha = 0;
            
            _sprite = new AnimationSprite("assets/tileset.png", 17, 9);
            _sprite.SetFrame(144);
            _sprite.SetOrigin(width / 2f, height / 2f);
            _fire = new AnimationSprite("assets/fire.png", 3, 1);
            AddChild(_sprite);
            AddChild(_fire);
            _fire.x = -width / 2f;
            _fire.y = -_fire.height + 14;
            reset();
        }

        private void reset()
        {
            _fire.visible = false;
            _sprite.SetColor(255f, 255f, 255f);
            _triggerCount = 0;
            _count = GXPEngine.Utils.Random(100, 200);
        }

        public new void Update()
        {
            base.Update();
          
            if (_count > 0)
                _count--;
            else
            {
                _fire.visible = true;
                _fire.SetFrame(_fireState.GetCount());
                _fireState.Update();
                _sprite.SetColor(255f, 0, 0);
                if (_triggerCount < _triggerTime)
                {
                    _triggerCount++;
                }
                else
                {
                    reset();
                }
            }
        }

        public bool isTriggered()
        {
            return _count == 0;
        }

        public override void onCollideWithEntity(Entity entity)
        {
        }

        public override void onCollideWithSolidTile(Tile tile)
        {
        }
    }
}