﻿namespace Game.entity
{
    public enum EntityType
    {
        Bullet,
        Player, 
        Key, 
        Zombie,
        Trap,
        Other,
        Chest
    }
}