﻿using System;
using System.Collections.Generic;
using Game.Map;
using GXPEngine;
using GXPEngine.Core;

namespace Game.entity
{
    /// <summary>
    ///  Represents every "living" object in the game
    /// </summary>
    public abstract class Creature : Entity
    {
        protected int maxHealth;
        protected int currentHealth;
        protected Direction _direction;
        protected Transformable goal;

        protected float engageSpeed = .35f;
        protected float idleSpeed = .2f;

        private Vector2 _idleVelocity;
        private int _idleCount;
        private int _idleMax;

        protected Creature(EntityType type, float x, float y, int maxHealth, float hitboxLR, float hitboxTD) : base(type, x, y, hitboxLR, hitboxTD)
        {
            _direction = Direction.SOUTH;
            this.maxHealth = maxHealth;
            currentHealth = maxHealth;
        }

        public Direction GetDirection()
        {
            return _direction;
        }

        public new void Update()
        {
            base.Update();

            if (type != EntityType.Player)
            {
                FindGoal();
                if (goal != null)
                {
                    MoveToGoal(engageSpeed);
                    _idleCount = 0;
                }
                else
                    Idle();
            }

            calcDirection();
            if (currentHealth <= 0)
            {
                Destroy();
                if (this is Player)
                {
                    MyGame.Instance().showEndScreen();
                }
            }
        }
        
        private void Idle()
        { 
            if (_idleCount == 0)
            {
                _idleMax = GXPEngine.Utils.Random(100, 200);
                _direction = (Direction) GXPEngine.Utils.Random(0, 4);
                _idleVelocity = Utils.GetDirectionAsVector(_direction).Multiply(idleSpeed);
            }
            if (_idleCount >= _idleMax)
            {
                _idleCount = 0;
            }
            else
            {
                hSpeed += _idleVelocity.x;
                vSpeed += _idleVelocity.y;
                _idleCount++;
            }
        }

        private void MoveToGoal(float speed)
        {
            if (x < goal.x)
                hSpeed += speed;
            else
                hSpeed -= speed;

            if (y < goal.y)
                vSpeed += speed;
            else
                vSpeed -= speed;
        }

        /// <summary>
        /// Gets called in base's Update method
        /// </summary>
        protected abstract void FindGoal();

        private void calcDirection()
        {
            if (Math.Abs(vSpeed) > 0 || Math.Abs(hSpeed) > 0)
            {
                if (Math.Abs(hSpeed) > Math.Abs(vSpeed))
                {
                    _direction = hSpeed > 0 ? Direction.EAST : Direction.WEST;
                }
                else
                {
                    _direction = vSpeed > 0 ? Direction.SOUTH : Direction.NORTH;
                }
            }
        }

        public bool CanSee(Transformable transformable)
        {
            // using distance but not squared
            if (!(transformable.DistanceTo(this) < 75000)) return false;
            foreach (Tile tile in Utils.GetAllTilesBetween(MyGame.Instance().GetCurrentRoom(), GetPosition(), transformable.GetPosition()))
            {
                if (tile != null && tile.IsSolid())
                    return false;
            }
            return true;
        }

        public int GetCurrentHealth()
        {
            return currentHealth;
        }

        public int GetMaxHealth()
        {
            return maxHealth;
        }

        public Transformable GetGoal()
        {
            return goal;
        }
    }
}