﻿using System;
using Game.Map;
using GXPEngine;
using GXPEngine.Core;

namespace Game.entity
{
    public class Zombie : Creature
    {
        public static readonly EntityState RUNNING_NORTH =
            new EntityState(0, 3, 10, pattern: new int[] {0, 1, 2, 3, 2, 1});

        public static readonly EntityState RUNNING_EAST =
            new EntityState(4, 7, 10, pattern: new int[] {4, 5, 6, 7, 6, 5});

        public static readonly EntityState RUNNING_SOUTH =
            new EntityState(8, 11, 10, pattern: new int[] {8, 9, 10, 11, 10, 9});

        public static readonly EntityState RUNNING_WEST =
            new EntityState(12, 15, 10, pattern: new int[] {12, 13, 14, 15, 14, 13});

        public static readonly EntityState IDLING_NORTH = new EntityState(0, 0, 5);
        public static readonly EntityState IDLING_EAST = new EntityState(4, 4, 5);
        public static readonly EntityState IDLING_SOUTH = new EntityState(8, 8, 5);
        public static readonly EntityState IDLING_WEST = new EntityState(12, 12, 5);

        private static readonly Sound _enemyDieSound1 = new Sound("assets/audio/enemy_die_1.mp3");
        private static readonly Sound _enemyDieSound2 = new Sound("assets/audio/enemy_die_2.mp3");
        private static readonly Sound _enemyDieSound3 = new Sound("assets/audio/enemy_die_3.mp3");

        private const int _attackDelay = 10;
        private int _attackDelayCount = _attackDelay;
        private EntityState _state;
        private readonly AnimationSprite _sprite;

        private ZombieData _data;

        public Zombie(ZombieData data) : base(EntityType.Zombie, data.GetPos().x, data.GetPos().y, 1, 0, 0)
        {
            _data = data;
            _sprite = new AnimationSprite("assets/enemy2.png", 4, 4);
            _sprite.SetOrigin(width / 2f, height / 2f);
            AddChild(_sprite);
            _direction = Direction.EAST;
            collider = new BoxCollider(this);
            _state = RUNNING_SOUTH;
        }


        public new void Update()
        {
            base.Update();
            if (_attackDelayCount > 0)
            {
                _attackDelayCount--;
            }
            else
            {
                _attackDelayCount = _attackDelay;
            }
            animate();
        }

        private void animate()
        {
            if (Math.Abs(hSpeed) > 0.5 || Math.Abs(vSpeed) > 0.5)
            {
                switch (_direction)
                {
                    case Direction.NORTH:
                        entityState = RUNNING_NORTH;
                        break;
                    case Direction.SOUTH:
                        entityState = RUNNING_SOUTH;
                        break;
                    case Direction.EAST:
                        entityState = RUNNING_EAST;
                        break;
                    case Direction.WEST:
                        entityState = RUNNING_WEST;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            else
            {
                switch (_direction)
                {
                    case Direction.NORTH:
                        entityState = IDLING_NORTH;
                        break;
                    case Direction.SOUTH:
                        entityState = IDLING_SOUTH;
                        break;
                    case Direction.EAST:
                        entityState = IDLING_EAST;
                        break;
                    case Direction.WEST:
                        entityState = IDLING_WEST;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            _sprite.SetFrame(entityState.GetCount());
        }

        public override void onCollideWithEntity(Entity entity)
        {
            if (entity is Bullet)
            {
                int randomSound = GXPEngine.Utils.Random(0, 3);
                switch (randomSound)
                {
                    case(0):
                        _enemyDieSound1.Play();
                        break;
                    case(1):
                        _enemyDieSound2.Play();
                        break;
                    case(2):
                        _enemyDieSound3.Play();
                        break;
                }
                MyGame.Instance().GetCurrentRoom().GetRoomData().GetZombies().Remove(_data);
                Destroy();
            }
        }

        public override void onCollideWithSolidTile(Tile tile)
        {
            _direction = (Direction) GXPEngine.Utils.Random(0, 4);
        }

        protected override void FindGoal()
        {
            if (goal == null)
            {
                goal = CanSee(MyGame.Instance().GetPlayer()) ? MyGame.Instance().GetPlayer() : null;
            }
            else
            {
                if (!CanSee(goal))
                    goal = null;
            }
        }

        public class ZombieData
        {
            private Vector2 _pos;

            public ZombieData(Vector2 pos)
            {
                _pos = pos;
            }

            public Vector2 GetPos()
            {
                return _pos;
            }
        }
    }
}