﻿using Game.Map;
using GXPEngine;
using GXPEngine.Core;

namespace Game.entity
{
    public class Bullet : Entity
    {
        private Vector2 _direction;
        private const int maxLiveTime = 250;
        private int liveTime;
        private readonly Creature owner;

        public Bullet(EntityType type, Creature owner, Vector2 vector2) : base(type, owner.x, owner.y, 0, 0, false)
        {
            _direction = vector2;
            this.owner = owner;
            AnimationSprite spr = new AnimationSprite("assets/grey.png", 1, 1);
            SetOrigin(width / 4f, height / 2f);
            AddChild(spr);
        }

        public Creature GetOwner()
        {
            return owner;
        }

        public new void Update()
        {
            base.Update();
            if (liveTime > maxLiveTime)
            {
                Destroy();
            }
            else
            {
                TryMove(_direction.x, _direction.y);
            }
            liveTime++;
        }

        public override void onCollideWithEntity(Entity entity)
        {
            if (!entity.Equals(owner))
                Destroy();
        }

        public override void onCollideWithSolidTile(Tile tile)
        {
            Destroy();
        }
    }
}