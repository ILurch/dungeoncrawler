﻿using System;
using System.Collections.Generic;
using Game.Map;
using GXPEngine;
using GXPEngine.Core;

namespace Game.entity
{
    /// <summary>
    /// Represents everything visible exept tiles in the game
    /// </summary>
    public abstract class Entity : Sprite
    {
        protected readonly EntityType type;
        protected new BoxCollider collider;
        protected float hSpeed;
        protected float vSpeed;
        protected EntityState entityState;
        protected Hitbox hitbox;
        protected bool colisionEntity;
        protected bool colisionTile;

        /// <summary>
        /// Hitbox float parameters are the margins to cut the sprite into a smaller hitbox
        /// </summary>
        /// <param name="type"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="room"></param>
        /// <param name="hitboxLR"></param>
        /// <param name="hitboxTD"></param>
        protected Entity(EntityType type, float x, float y, float hitboxLR, float hitboxTD,
            bool colisionEntity = true, bool colisionTile = true) : base(
            "assets/shadow.png")
        {
            this.type = type;
            this.x = x;
            this.y = y;
            this.colisionEntity = colisionEntity;
            this.colisionTile = colisionTile;
            collider = new BoxCollider(this);
            SetOrigin(width / 2f, height / 2f);
            // Adding transparency to the shadow
            alpha = 150f;
            entityState = EntityState.BASIC;
            hitbox = new Hitbox(hitboxLR, hitboxTD);
        }

        public void Update()
        {
            TryMove(hSpeed, 0);
            TryMove(0, vSpeed);
            hSpeed *= 0.8f;
            vSpeed *= 0.8f;

            const float tolerance = 0.005f;
            if (Math.Abs(hSpeed) < tolerance)
                hSpeed = 0;
            if (Math.Abs(vSpeed) < tolerance)
                vSpeed = 0;
            entityState.Update();
        }

        protected void TryMove(float x, float y)
        {
            this.x += x;
            this.y += y;

            foreach (GameObject obj in GetCollisions())
            {
                if (!colisionEntity && obj is Entity) return;
                if (!colisionTile && obj is Tile) return;
                if (checkOverlap((Sprite) obj))
                {
                    if (obj is Entity && colisionEntity)
                    {
                        if (((Entity) obj).colisionEntity)
                        {
                            this.x -= x;
                            this.y -= y;
                        }
                        onCollideWithEntity(obj as Entity);
                        return;
                    }
                    if (obj is Tile && ((Tile) obj).IsSolid() && colisionTile)
                    {
                        this.x -= x;
                        this.y -= y;
                        onCollideWithSolidTile(obj as Tile);
                        return;
                    }
                }
            }
        }

        // (X2' >= X1 && X1' <= X2) && (Y2' >= Y1 && Y1' <= Y2)

        // overlap check method
        private bool checkOverlap(Sprite spr)
        {
            float x1 = x - (width / 2f) - hitbox.GetLeftRightMargin();
            float x2 = x + (width / 2f) + hitbox.GetLeftRightMargin();
            float y1 = y - (height / 2f) - hitbox.GetTopDownMargin();
            float y2 = y + (height / 2f) + hitbox.GetTopDownMargin();

            float xCheck1 = spr.x;
            float xCheck2 = spr.x + spr.width;
            float yCheck1 = spr.y;
            float yCheck2 = spr.y + spr.height;

            var ent = spr as Entity;
            if (ent != null)
            {
                Entity entity = ent;
                xCheck1 -= (entity.width / 2f) - entity.hitbox.GetLeftRightMargin();
                xCheck2 += (entity.width / 2f) + entity.hitbox.GetLeftRightMargin();
                yCheck1 -= (entity.height / 2f) - entity.hitbox.GetTopDownMargin();
                yCheck2 += (entity.height / 2f) + entity.hitbox.GetTopDownMargin();
            }

            return (xCheck2 >= x1 && xCheck1 <= x2) && (yCheck2 >= y1 && yCheck1 <= y2);
        }

        public abstract void onCollideWithEntity(Entity entity);

        public abstract void onCollideWithSolidTile(Tile tile);

        public EntityType GetEntityType()
        {
            return type;
        }
    }
}