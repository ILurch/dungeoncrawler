﻿using System.Security.Cryptography.X509Certificates;
using Game.Map;
using GXPEngine;

namespace Game.entity
{
    public class Key : Entity
    {

        public class KeyData
        {
            private int _x;
            private int _y;
            private int _openLock;

            public KeyData(int x, int y, int openLock)
            {
                _x = x;
                _y = y;
                _openLock = openLock;
            }

            public int GetX()
            {
                return _x;
            }

            public int GetY()
            {
                return _y;
            }

            public int GetOpenLock()
            {
                return _openLock;
            }
        }
        
        private int _openLock;
        private AnimationSprite _sprite;
        private float _idleSpeed = 0.15f;
        private int _idleCount;
        private bool _upwards;
        private KeyData _data;

        public Key(KeyData data) : base(EntityType.Key, data.GetX(), data.GetY(), 0, 0, false, false)
        {
            _data = data;
            _openLock = data.GetOpenLock();
            _sprite = new AnimationSprite("assets/tileset.png", 17, 9);
            _sprite.SetFrame(142);
            _sprite.SetOrigin(width / 2f, height / 2f);
            AddChild(_sprite);
            SetScaleXY(1.5f, 1.5f);
        }

        public void Update()
        {
            if (_idleCount == 50 || _idleCount == 0)
            {
                _upwards = !_upwards;
            }
            if (_upwards)
            {
                _sprite.Move(0, _idleSpeed);
                _idleCount++;
            }
            else
            {
                _sprite.Move(0, -1 * _idleSpeed);
                _idleCount--;
            }
        }

        public KeyData GetData()
        {
            return _data;
        }

        public override void onCollideWithEntity(Entity entity)
        {
            throw new System.NotImplementedException();
        }

        public override void onCollideWithSolidTile(Tile tile)
        {
            throw new System.NotImplementedException();
        }

        public int GetLock()
        {
            return _openLock;
        }
    }
}