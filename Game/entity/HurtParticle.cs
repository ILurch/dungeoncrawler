﻿using System.Security.Cryptography.X509Certificates;
using Game.Map;
using GXPEngine;

namespace Game.entity
{
    public class HurtParticle : AnimationSprite
    {
        private float _xSpeed;
        private float _ySpeed;
        private float _direction;
        private int _lifeCount;
        private int _lifeTime;
        
        public HurtParticle(float x, float y) : base (Tile.TileSetImage, 17, 9)
        {
            SetFrame(146);
            SetScaleXY(0.4f, 0.4f);
            _xSpeed = GXPEngine.Utils.Random(0.75f, 1.25f);
            _ySpeed = GXPEngine.Utils.Random(0.75f, 1.25f);
            _direction = GXPEngine.Utils.Random(0, 360);
            _lifeTime = GXPEngine.Utils.Random(10, 20);
        }

        public void Update()
        {
            if (_lifeCount > _lifeTime)
            {
                Destroy();
                
            }
            x += _xSpeed;
            y += _ySpeed;
            
            _lifeCount++;
            
        }
    }
}