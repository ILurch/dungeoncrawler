﻿using System;
using System.Collections.Generic;
using Game.Map;

namespace Game.entity
{
    public class Inventory : Entity
    {
        private string _title;
        private Dictionary<int, ItemStack> _items;
        private readonly int _size;

        public Inventory(string title, int size) : base(EntityType.Other, 0, 0, 0, 0)
        {
            this._title = title;
            _items = new Dictionary<int, ItemStack>();
            this._size = size;
        }

        public new void Update()
        {
            base.Update();
        }

        public override void onCollideWithEntity(Entity entity)
        {
        }

        public override void onCollideWithSolidTile(Tile tile)
        {
            // totally ignored
        }

        public ItemStack GetItem(int index)
        {
            return _items.ContainsKey(index) ? _items[index] : null;
        }

        public void SetItem(int index, ItemStack itemStack)
        {
            if (index >= 0 && index < _size)
            {
                _items[index] = itemStack;
            }
        }

        public void AddItem(ItemStack addition)
        {
            foreach (int i in _items.Keys)
            {
                ItemStack itemStack = _items[i];
                transferItemStack(addition, itemStack);
            }
            if (addition.Amount > 0)
            {
                for (int i = 0; i < _size; i++)
                {
                    if (!_items.ContainsKey(i))
                    {
                        _items.Add(i, addition);
                    }
                }
            }
        }

        private void transferItemStack(ItemStack from, ItemStack to)
        {
            if (from.GetMaterial().Equals(to.GetMaterial()))
            {
                to.Amount += from.Amount;
                to.Amount = 0;
            }
        }

        public int GetSize()
        {
            return _size;
        }
    }
}