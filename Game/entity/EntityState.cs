﻿using System;
using System.Threading;
using GXPEngine;
using GXPEngine.Core;

namespace Game.entity
{
    public class EntityState
    {
        public static readonly EntityState BASIC = new EntityState(0, 0);

        private readonly int _speed;
        private int _frames;
        private int _count;
        private int _speedCount;
        private Vector2 _indexes;
        private int _delay;
        private int _delayCount;
        private int _minDelay;
        private int _maxDelay;

        private int[] _pattern;
        
        /// <summary>
        /// Speed needs to be greater than 0 or you will get an infinite loop in the Update method's counter
        /// </summary>
        /// <param name="start"></param>
        /// <param name="stop"></param>
        /// <param name="speed"></param>
        public EntityState(int start, int stop, int speed = 4, int minDelay = 0, int maxDelay = 0, int[] pattern = null)
        {
            _indexes = new Vector2(start, stop);
            _speed = speed;
            _delay = Mathf.IntRange(minDelay, maxDelay);
            _minDelay = minDelay;
            _maxDelay = maxDelay;
            _pattern = pattern;
        }

        /// <summary>
        /// Needs to be updated in class that uses Animation
        /// </summary>
        public void Update()
        {
            if (_delayCount < _delay && _delay > 0)
            {
                _delayCount++;
                return;
            }
            
            if (_indexes.x == _indexes.y)
                return;
            if (_speedCount >= _speed)
            {
                if (_count >= _indexes.Size)
                {
                    _count = 0;
                    _delayCount = 0;
                    _delay = Mathf.IntRange(_minDelay, _maxDelay);
                }
                else
                {
                    _count++;
                }
                _speedCount = 0;
            }
            _speedCount++;
        }

        public void Reset()
        {
            _count = 0;
        }

        public int GetCount()
        {
            if (_pattern != null)
                return _pattern[_count];
            return _count + (int) _indexes.x;
        }

        /// <summary>
        /// Returns the start and the stop values
        /// </summary>
        /// <returns></returns>
        public Vector2 GetIndexes()
        {
            return _indexes;
        }
    }
}