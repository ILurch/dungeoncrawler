﻿using System;
using System.Runtime.Remoting.Messaging;
using Game.Map;
using GXPEngine;

namespace Game.entity
{
    public class Player : Creature
    {
        public static readonly EntityState RUNNING_SOUTH = new EntityState(0, 2, 5);
        public static readonly EntityState RUNNING_EAST = new EntityState(9, 11, 5);
        public static readonly EntityState RUNNING_NORTH = new EntityState(3, 5, 5);
        public static readonly EntityState RUNNING_WEST = new EntityState(6, 8, 5);
        public static readonly EntityState IDLING_NORTH = new EntityState(3, 3, 3, 5, 125);
        public static readonly EntityState IDLING_SOUTH = new EntityState(0, 0, 3, 5, 125);
        public static readonly EntityState IDLING_EAST = new EntityState(9, 9, 3, 5, 125);
        public static readonly EntityState IDLING_WEST = new EntityState(6, 6, 3, 5, 125);

        private static readonly Sound _walk1 = new Sound("assets/audio/walk_1.mp3");
        private static readonly Sound _walk2 = new Sound("assets/audio/walk_2.mp3");
        private static readonly Sound _walk3 = new Sound("assets/audio/walk_3.mp3");
        private static readonly Sound _hurt1 = new Sound("assets/audio/hurt_1.mp3");
        private static readonly Sound _hurt2 = new Sound("assets/audio/hurt_2.mp3");
        private static readonly Sound _hurt3 = new Sound("assets/audio/hurt_3.mp3");
        private static readonly Sound _keyPickup = new Sound("assets/audio/key_pickup.mp3");

        private AnimationSprite _sprite;
        private MyGame _game;
        private Weapon _gun;


        private int _walkSoundCount;
        private const int _walkSoundSpeed = 10;
        private int _lastWalkSound;

        private int _lastHurtSound;
        private SoundChannel _hurtSoundChannel;

        private const float _speed = .65f;

        private bool _hasWeapon;

        public Player(float x, float y, MyGame game) : base(EntityType.Player, x, y, 200, 0, 0)
        {
            _sprite = new AnimationSprite("assets/player.png", 6, 2);
            _direction = Direction.SOUTH;
            _game = game;
            _sprite.SetOrigin(_sprite.width / 2f, _sprite.height / 2f);
            AddChild(_sprite);
            animate();
            _gun = new Weapon(this, 32);
            entityState = IDLING_SOUTH;
        }

        public new void Update()
        {
            //Console.WriteLine(x + " " + y);
            base.Update();
            movement();
            animate();
            walkSound();
            //playerDropItem();
            PlayerUseWeapon();
            if (currentHealth > 0)
                _game.GetHUD().UpdateHud(currentHealth, _gun.CurrentAmmo, (int) x, (int) y);
        }

        private void walkSound()
        {
            if (Math.Abs(hSpeed) > 0.5 || Math.Abs(vSpeed) > 0.5)
            {
                if (_walkSoundCount > _walkSoundSpeed)
                {
                    int randomSound;
                    do
                    {
                        randomSound = GXPEngine.Utils.Random(0, 3);
                    } while (randomSound == _lastWalkSound);
                    _lastWalkSound = randomSound;
                    switch (randomSound)
                    {
                        case(0):
                            _walk1.Play();
                            break;
                        case(1):
                            _walk2.Play();
                            break;
                        case(2):
                            _walk3.Play();
                            break;
                    }
                    _walkSoundCount = 0;
                }
                else
                    _walkSoundCount++;
            }
            else
            {
                _walkSoundCount = 0;
            }
        }

        public override void onCollideWithEntity(Entity entity)
        {
            if (entity is Zombie || (entity is Trap && ((Trap) entity).isTriggered()))
            {
                // play hurt sound

                currentHealth--;
                // HurtParticle particle = new HurtParticle(x, y);
                // _game.GetCurrentRoom().AddChild(particle);
                if (_hurtSoundChannel == null || !_hurtSoundChannel.IsPlaying)
                {
                    int randomSound = 0;
                    /*do
                    {
                        randomSound = GXPEngine.Utils.Random(0, 3);
                    } while (randomSound == _lastHurtSound);
                    _lastHurtSound = randomSound;*/
                    switch (randomSound)
                    {
                        case(0):
                            _hurtSoundChannel = _hurt1.Play();
                            break;
                        case(1):
                            _hurtSoundChannel = _hurt2.Play();
                            break;
                        case(2):
                            _hurtSoundChannel = _hurt3.Play();
                            break;
                    }
                }
            }
            else if (entity is Key)
            {
                _keyPickup.Play();
                Key key = (Key) entity;
                foreach (Door door in _game.GetDoors())
                {
                    if (door.GetDestination() == key.GetLock())
                    {
                        door.Locked = false;
                        _game.GetHUD().Log("[Info] You can now access another room.");
                        key.Destroy();
                    }
                }
                _game.GetCurrentRoom().GetRoomData().GetKeys().Remove(key.GetData()); // removes item from room
            }
            else if (entity is Chest)
            {
                Chest chest = (Chest) entity;
                if (!chest.GetData().IsEmpty())
                {
                    chest.GetData().SetEmpty(true);
                    string logLine = "You received ";
                    if (!_hasWeapon)
                    {
                        logLine += "1x Boomstick ";
                        _hasWeapon = true;
                    }
                    foreach (ItemStack itemStack in chest.getContent().GetItems().ToArray())
                    {
                        switch (itemStack.GetMaterial())
                        {
                            case(ItemType.Bullet):
                                logLine += itemStack.Amount + "x " + itemStack.GetName() + " ";
                                _gun.CurrentAmmo += itemStack.Amount;
                                break;
                            case(ItemType.Health):
                                logLine += itemStack.Amount + "x Healthpoints ";
                                currentHealth += itemStack.Amount;
                                break;
                            case(ItemType.Trap):
                                logLine += itemStack.Amount + "x " + itemStack.GetName() + " ";
                                break;
                        }
                        chest.getContent().GetItems().Remove(itemStack);
                    }
                    _game.GetHUD().Log(logLine);
                }
            }
        }

        public override void onCollideWithSolidTile(Tile tile)
        {
            if (tile.GetMaterial().ToString().StartsWith("DOOR"))
            {
                foreach (Door door in _game.GetCurrentRoom().GetDoors())
                {
                    if (tile.GetPosition().Equals(door.GetPosition()))
                    {
                        if (door.Locked && door.GetDestination() > _game.GetCurrentRoom().GetId())
                        {
                            _game.GetHUD().Log("[Info] This door is locked.");
                        }
                        else
                        {
                            _game.GetCurrentRoom().GetRoomData().SetPlayerSpawn(x, y);
                            _game.changeRoom(door.GetDestination());
                        }
                        return;
                    }
                }
            }
        }

        protected override void FindGoal()
        {
            // not needed
        }

        private void PlayerUseWeapon()
        {
            if (_hasWeapon)
            {
                if (Input.GetKeyDown(GXPEngine.Key.SPACE))
                {
                    _gun.holdTrigger();
                }
                else if (Input.GetKeyUp(GXPEngine.Key.SPACE))
                {
                    _gun.releaseTrigger();
                }
            }
        }

        /* private void playerDropItem()
         {
             if (Input.GetKeyDown(Key.Q))
             {
                 _game.GetHUD().Log("Test");
                 Vector2 vector = Utils.GetDirectionAsVector(_direction);
                 ItemStack itemStack = _hotbar[_hotbarSelection];
                 if (itemStack != null && itemStack.Amount > 0)
                 {
                     itemStack.Amount--;
                     Item item = new Item(itemStack, x, y, room);
                     vector.Multiply(30);
                     item.SetSpeed(vector.x, vector.y);
                     _game.GetCurrentRoom().GetEntities().Add(item);
                     _game.GetCurrentRoom().AddChild(item);
                 }
             }
         }*/

        // make an own class for animations
        private void animate()
        {
            if (Math.Abs(hSpeed) > 0.5 || Math.Abs(vSpeed) > 0.5)
            {
                switch (_direction)
                {
                    case Direction.NORTH:
                        entityState = RUNNING_NORTH;
                        break;
                    case Direction.SOUTH:
                        entityState = RUNNING_SOUTH;
                        break;
                    case Direction.EAST:
                        entityState = RUNNING_EAST;
                        break;
                    case Direction.WEST:
                        entityState = RUNNING_WEST;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            else
            {
                switch (_direction)
                {
                    case Direction.NORTH:
                        entityState = IDLING_NORTH;
                        break;
                    case Direction.SOUTH:
                        entityState = IDLING_SOUTH;
                        break;
                    case Direction.EAST:
                        entityState = IDLING_EAST;
                        break;
                    case Direction.WEST:
                        entityState = IDLING_WEST;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            _sprite.SetFrame(entityState.GetCount());
        }

        private void movement()
        {
            if (Input.GetKey(GXPEngine.Key.W))
                vSpeed -= _speed;
            if (Input.GetKey(GXPEngine.Key.S))
                vSpeed += _speed;
            if (Input.GetKey(GXPEngine.Key.A))
                hSpeed -= _speed;
            if (Input.GetKey(GXPEngine.Key.D))
                hSpeed += _speed;
        }
    }
}