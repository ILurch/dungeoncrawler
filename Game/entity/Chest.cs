﻿using System.Collections.Generic;
using System.Security.Policy;
using Game.Map;
using GXPEngine;

namespace Game.entity
{
    // Chest Object on
    public class Chest : Entity
    {
        public class ChestContent
        {
            private List<ItemStack> _items;

            /// <summary>
            /// The higher the level is the more items will be in it
            /// </summary>
            /// <param name="level"></param>
            public ChestContent(int level, bool firstChest = false)
            {
                _items = new List<ItemStack>();

                ItemStack bullets = new ItemStack(Game.ItemType.Bullet, 6, "Bullet(s) of Doom");
                ItemStack health = new ItemStack(Game.ItemType.Health, 25, "Health");
                ItemStack traps = new ItemStack(Game.ItemType.Trap, 3, "Devil's trap(s)");

                bullets.Amount = 6 + (int) GXPEngine.Utils.Random(0, level * 1.5f);
                health.Amount = 20 + (int) GXPEngine.Utils.Random(0, level * 1.5f);
                traps.Amount = 3 + (int) GXPEngine.Utils.Random(0, level * 1.5f);

                _items.Add(health);
                _items.Add(traps);
                _items.Add(bullets);
            }

            public List<ItemStack> GetItems()
            {
                return _items;
            }
        }

        public class ChestData
        {
            private int _x;
            private int _y;
            private int _level;
            private bool _empty;

            public ChestData(int x, int y, int level)
            {
                _x = x;
                _y = y;
                _level = level;
            }

            public int GetX()
            {
                return _x;
            }

            public int GetY()
            {
                return _y;
            }

            public int GetLevel()
            {
                return _level;
            }

            public bool IsEmpty()
            {
                return _empty;
            }

            public void SetEmpty(bool empty)
            {
                _empty = empty;
            }
        }

        private ChestContent _content;
        private ChestData _data;
        private AnimationSprite _leftChest;
        private AnimationSprite _rightChest;

        public Chest(ChestData data) : base(
            EntityType.Chest, data.GetX(), data.GetY(), 0, 0, true, false)
        {
            _data = data;
            _leftChest = new AnimationSprite(Tile.TileSetImage, 17, 9);
            _leftChest.SetFrame(147);
            _rightChest = new AnimationSprite(Tile.TileSetImage, 17, 9);
            _rightChest.SetFrame(148);
            AddChild(_leftChest);
            AddChild(_rightChest);
            _leftChest.SetOrigin(32, height / 2f);
            _rightChest.SetOrigin(0, height / 2f);
            _content = new ChestContent(data.GetLevel());
        }

        public ChestData GetData()
        {
            return _data;
        }

        public ChestContent getContent()
        {
            return _content;
        }

        public bool IsEmpty()
        {
            return _content.GetItems().Count == 0;
        }

        public override void onCollideWithEntity(Entity entity)
        {
        }

        public override void onCollideWithSolidTile(Tile tile)
        {
        }
    }
}