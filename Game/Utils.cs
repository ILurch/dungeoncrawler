﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Game.entity;
using Game.Map;
using GXPEngine.Core;

namespace Game
{
    public class Utils
    {
        /* public static readonly SpriteMap materials = new SpriteMap("assets/tileset.png", 9, 17);

        public static Bitmap GetMaterialSprite(int index)
        {
            return materials.GetFrame(index);
        }
        */

        public static Vector2 GetDirectionAsVector(Direction dir)
        {
            var v = new Vector2(0, 0);
            switch (dir)
            {
                case Direction.NORTH:
                    v.y = -1;
                    break;
                case Direction.SOUTH:
                    v.y = 1;
                    break;
                case Direction.EAST:
                    v.x = 1;
                    break;
                case Direction.WEST:
                    v.x = -1;
                    break;
            }
            return v;
        }

        /*public static void SaveRoom(Room room)
        {
            string filename = "rooms/" + room.GetPosition().ToString() + ".room";
            List<string> lines = new List<string>();
            Directory.CreateDirectory("rooms/");
            lines.Add(room.GetWidth() + " " + room.GetHeight());

            // Saving tiles
            for (int x = 0; x < room.GetTiles().GetLength(0); x++)
            {
                string line = "";
                for (int y = 0; y < room.GetTiles().GetLength(1); y++)
                {
                    Map.Material material = room.GetTiles()[x, y].GetMaterial();
                    if (material == Map.Material.AIR)
                        line += " ";
                    else
                        line += (int) room.GetTiles()[x, y].GetMaterial();
                }
                lines.Add(line);
            }

            // saving entities
            foreach (var ent in room.GetEntities())
            {
                string entityLine = "";
                switch (ent.GetEntityType())
                {
                    case EntityType.Other:
                        // ignoring this type
                        break;
                    case EntityType.Player:
                        // ignore this type
                        break;
                    default:
                        entityLine += ent.GetEntityType() + ";" + ent.GetPosition().ToString() + ";";
                        if (ent.GetType() == typeof(Creature))
                        {
                            var cr = (Creature) ent;
                            entityLine += cr.GetMaxHealth() + ";" + cr.GetCurrentHealth();
                        }
                        else if (ent.GetType() == typeof(Item))
                        {
                            var item = (Item) ent;
                            entityLine += item.GetItemStack().GetMaterial() + ";" + item.GetItemStack().Amount;
                        }
                        lines.Add(entityLine);
                        break;
                }
            }
            File.WriteAllLines(filename, lines);
        }*/

        public static RoomData LoadRoomData(int pos)
        {
            string filename = "rooms/" + pos.ToString() + ".room";
            string[] lines = File.ReadAllLines(filename);
            int currentLine = 0;
            string[] playerPosData = lines[currentLine++].Split(' ');
            Vector2 playerPos = new Vector2(int.Parse(playerPosData[0]), int.Parse(playerPosData[1]));

            string[] size = lines[currentLine++].Split(' ');
            int[,] arr = new int[int.Parse(size[0]), int.Parse(size[1])];

            string[] doorLine = lines[currentLine++].Split(';');
            List<Door> doors = new List<Door>();

            foreach (string s in doorLine)
            {
                string[] doorData = s.Split(' ');
                Door door = new Door(int.Parse(doorData[2]), int.Parse(doorData[0]), int.Parse(doorData[1]));
                doors.Add(door);
            }

            string[] zombieLine = lines[currentLine++].Split(';');
            List<Zombie.ZombieData> zombies = new List<Zombie.ZombieData>();
            if (zombieLine[0].Trim().Length > 0)
                foreach (string s in zombieLine)
                {
                    string[] zombieData = s.Split(' ');
                    Vector2 z = new Vector2(int.Parse(zombieData[0]), int.Parse(zombieData[1]));
                    zombies.Add(new Zombie.ZombieData(z));
                }

            string[] trapLine = lines[currentLine++].Split(';');
            List<Vector2> traps = new List<Vector2>();
            if (trapLine[0].Trim().Length > 0)
                foreach (string s in trapLine)
                {
                    string[] trapData = s.Split(' ');
                    Vector2 z = new Vector2(int.Parse(trapData[0]), int.Parse(trapData[1]));
                    traps.Add(z);
                }

            string[] keyLine = lines[currentLine++].Split(';');
            List<Key.KeyData> keys = new List<Key.KeyData>();
            if (keyLine[0].Trim().Length > 0)
                foreach (string s in keyLine)
                {
                    string[] keyData = s.Split(' ');
                    Key.KeyData z = new Key.KeyData(int.Parse(keyData[0]), int.Parse(keyData[1]),
                        int.Parse(keyData[2]));
                    keys.Add(z);
                }

            string[] chestLine = lines[currentLine++].Split(';');
            List<Chest.ChestData> chests = new List<Chest.ChestData>();
            if (chestLine[0].Trim().Length > 0)
                foreach (string s in chestLine)
                {
                    string[] chestData = s.Split(' ');
                    var chest = new Chest.ChestData(int.Parse(chestData[0]), int.Parse(chestData[1]),
                        int.Parse(chestData[2]));
                    chests.Add(chest);
                }

            for (var x = currentLine; x < lines.Length; x++)
            {
                string[] lineData = lines[x].Split(' ');
                for (var y = 0; y < arr.GetLength(1); y++)
                {
                    try
                    {
                        int id = int.Parse(lineData[y]);
                        arr[x - 7, y] = id;
                    }
                    catch (IndexOutOfRangeException ex0)
                    {
                    }
                    catch (FormatException ex)
                    {
                    }
                }
            }

            /* loading tiles
            for (int x = 2; x <= arr.GetLength(0); x++)
            {
                string[] lineArr = lines[x].Split(' ');
                for (int y = 0; y < arr.GetLength(1); y++)
                {
                    int tileId = 0;
                    try
                    {
                        tileId = int.Parse(lineArr[y].Trim());
                    }
                    catch (IndexOutOfRangeException ex0)
                    {
                    }
                    catch (FormatException ex)
                    {
                    }
                    arr[x - 1, y] = tileId;
                }
            }*/
            RoomData data = new RoomData(arr, playerPos, doors, zombies, traps, keys, chests);
            return data;

            //loading entities
            /*
            for (int x = arr.GetLength(0) + 1; x < lines.Length; x++)
            {
                string[] dataArr = lines[x].Split(';');
                EntityType type;
                Enum.TryParse(dataArr[0], out type);
                string[] posStrings = dataArr[1].Split(',');
                Vector2 position = new Vector2(float.Parse(posStrings[0]), float.Parse(posStrings[1]));
                switch (type)
                {
                    case EntityType.Bullet:
                        break;
                    case EntityType.Player:
                        break;
                    case EntityType.Item:
                        Material mat = (Material) Enum.Parse(typeof(Material), dataArr[2]);
                        ItemStack itemStack = new ItemStack(mat, int.Parse(dataArr[3]), mat.ToString());
                        Item item = new Item(itemStack, position.x, position.y, room);
                        room.AddEntity(item);
                        break;
                    case EntityType.Zombie:
                        break;
                    case EntityType.Other:
                        break;
                }
            }
             loading */
        }

        /// <summary>
        /// Inspired by Bresenham's line algorithm
        /// </summary>
        /// <param name="room"></param>
        /// <param name="start"></param>
        /// <param name="stop"></param>
        /// <returns></returns>
        public static List<Tile> GetAllTilesBetween(Room room, Vector2 start, Vector2 stop)
        {
            int x0 = (int) start.x;
            int y0 = (int) start.y;
            int x1 = (int) stop.x;
            int y1 = (int) stop.y;
            bool steep = Math.Abs(y1 - y0) > Math.Abs(x1 - x0);
            int temp = 0; // temporary storage for swapping values 

            if (steep)
            {
                temp = x0; // swap x0 and y0
                x0 = y0;
                y0 = temp;
                temp = x1; // swap x1 and y1
                x1 = y1;
                y1 = temp;
            }
            if (x0 > x1)
            {
                temp = x0; // swap x0 and x1
                x0 = x1;
                x1 = temp;
                temp = y0; // swap y0 and y1
                y0 = y1;
                y1 = temp;
            }
            int deltaX = x1 - x0;
            int deltaY = Math.Abs(y1 - y0);
            int error = deltaX / 2;
            int yStep = (y0 < y1) ? 1 : -1;
            int y = y0;
            List<Tile> tiles = new List<Tile>();
            for (int x = x0; x <= x1; x++)
            {
                int tileX = steep ? y : x;
                int tileY = steep ? x : y;
                Tile tile = room.GetTileRel(tileX, tileY);
                if (!tiles.Contains(tile))
                    tiles.Add(tile);
                error -= deltaY;
                if (error < 0)
                {
                    y += yStep;
                    error += deltaX;
                }
            }
            return tiles;
        }

        public static Color GetRandomColor()
        {
            return Color.FromArgb(GXPEngine.Utils.Random(0, 256), GXPEngine.Utils.Random(0, 256),
                GXPEngine.Utils.Random(0, 256));
        }
    }
}