using System;
using System.Collections.Generic;
using System.IO;
using System.Management.Instrumentation;
using Game.entity;
using Game.hud;
using Game.Map;
using GXPEngine;

namespace Game
{
    public class MyGame : GXPEngine.Game
    {
        private static MyGame instance;
        private static List<RoomData> rooms = new List<RoomData>();

        private GameCamera camera;
        private Room _room;
        private Player player;
        private HUD hud;

        private Sound _backgroundMusic = new Sound("assets/audio/Dungeon_in_a_Gamebox.mp3", true);
        private SoundChannel _channel;

        private Screen _screen;

        public MyGame() : base(800, 600, true)
        {
            _channel = _backgroundMusic.Play();
            _screen = new Screen("Press enter to start! Movement: WASD, Shoot: Spacebar", width, height);
            AddChild(_screen);
        }

        private void initialize()
        {
            camera = new GameCamera(this);
            _room = new Room();
            hud = new HUD(width, height);

            changeRoom(_room.GetId());
            setupRoom();
        }

        public void Update()
        {
            if (Input.GetKeyDown(GXPEngine.Key.M))
            {
                _channel.Mute = !_channel.Mute;
            }
            else if (Input.GetKeyDown(GXPEngine.Key.ENTER) && _screen != null)
            {
                player = null;
                RemoveChild(_screen);
                _screen = null;
                initialize();
            }
        }

        public void showEndScreen()
        {
            foreach (GameObject obj in GetChildren().ToArray())
            {
                obj.Destroy();
            }
            _screen = new Screen("You died!", width, height);
            AddChild(_screen);
        }

        private void setupRoom()
        {
            camera.AddChild(_room);
            AddChild(camera);
            AddChild(hud);
        }

        public void changeRoom(int id)
        {
            if (id >= 0 && id < rooms.Count)
            {
                foreach (GameObject obj in _room.GetChildren().ToArray())
                {
                    if (!(obj is Player))
                        obj.Destroy();
                }
                _room.UpdateFrom(id, rooms[id]);
                if (player == null)
                {
                    player = new Player(0, 0, this);
                    _room.AddEntity(player);
                }
                else
                {
                    // player is always on top
                    _room.RemoveChild(player);
                    _room.AddChild(player);
                }
                player.x = _room.GetRoomData().GetPlayerSpawn().x;
                player.y = _room.GetRoomData().GetPlayerSpawn().y;
            }
        }

        public HUD GetHUD()
        {
            return hud;
        }

        public static void Main()
        {
            string[] files = Directory.GetFiles("rooms/", "*.room");
            foreach (string s in files)
            {
                string name = s.Substring(6);
                name = name.Replace(".room", "");
                int x = int.Parse(name);
                RoomData room = Utils.LoadRoomData(x);
                rooms.Add(room);
            }
            instance = new MyGame();
            instance.Start();
        }

        public Room GetCurrentRoom()
        {
            return _room;
        }

        public Player GetPlayer()
        {
            return player;
        }

        public static MyGame Instance()
        {
            return instance;
        }

        public List<RoomData> GetRoomDatas()
        {
            return rooms;
        }

        public List<Door> GetDoors()
        {
            List<Door> doors = new List<Door>();
            foreach (RoomData data in rooms)
            {
                foreach (Door door in data.GetDoors())
                {
                    doors.Add(door);
                }
            }
            return doors;
        }
    }
}