﻿using System.Drawing;
using GXPEngine;

namespace Game.hud
{
    public class Dialog : Canvas
    {
        private string[] _lines;
        private const int _dialogSpeed = 1;
        private bool destroyed;

        public Dialog(string[] lines, int width, int height) : base(width, height)
        {
            _lines = lines;
        }

        public void Update(Graphics graphics)
        {
            if (!destroyed)
            {
                PointF _pointF = new PointF(x, y);
                //graphics.DrawString();
            }
        }

        public override void Destroy()
        {
            base.Destroy();
            destroyed = true;
        }
    }
}