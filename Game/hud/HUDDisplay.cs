﻿using System;
using System.Drawing;
using Game.Map;
using GXPEngine;

namespace Game.hud
{
    public class HUDDisplay : Canvas
    {
        private AnimationSprite _sprite;
        private Font _font;
        private SolidBrush _brush;

        public HUDDisplay(DisplayType type) : base(32, 64)
        {
            _sprite = new AnimationSprite(Tile.TileSetImage, 17, 9);
            _font = new Font("assets/fonts/arial.ttf", 18, FontStyle.Regular);
            _brush = new SolidBrush(Color.BurlyWood);
            if (type == DisplayType.AMMO)
                _sprite.SetFrame(145);
            else if (type == DisplayType.HEALTH)
                _sprite.SetFrame(146);
            AddChild(_sprite);
            _sprite.y = 32;
        }

        // graphics.DrawString(message, font, defaultBrush, position);
        public void Update(Graphics graphics, int number)
        {
            graphics.DrawString(number + "x", _font, _brush, x - 5, y);
        }

        public enum DisplayType
        {
            AMMO,
            HEALTH
        }
    }
}