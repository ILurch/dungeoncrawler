﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.Security.Cryptography;
using Game.hud;
using GXPEngine;

namespace Game.hud
{
    /// <summary>
    /// Gives feedback to the player on his performance
    /// </summary>
    public class HUD : Canvas
    {
        private SolidBrush defaultBrush;
        private Font font;
        private PointF position;
        private PrivateFontCollection collection = new PrivateFontCollection();

        private Font _logFont;
        private List<LogString> _logs;
        private int logCount;

        private bool dead;

        private HUDDisplay _ammo;
        private HUDDisplay _health;

        // TODO Log output on screen

        public HUD(int width, int height) : base(width, height)
        {
            _ammo = new HUDDisplay(HUDDisplay.DisplayType.AMMO);
            _health = new HUDDisplay(HUDDisplay.DisplayType.HEALTH);
            defaultBrush = new SolidBrush(Color.White);
            collection.AddFontFile("assets/fonts/arial.ttf");
            font = new Font(collection.Families[0], 12, FontStyle.Regular);
            position = new PointF(10, 10);
            _logFont = new Font(collection.Families[0], 12, FontStyle.Regular);
            _logs = new List<LogString>();

            AddChild(_ammo);
            AddChild(_health);
            _ammo.SetXY(game.width / 2f - 25, game.height - 70);
            _health.SetXY(game.width / 2f + 25, game.height - 70);
        }

        public void UpdateHud(int health, int ammo, int playerX, int playerY)
        {
            string message = "FPS: " + game.currentFps + "\nPlayerXY(X=" + playerX + "|Y=" + playerY + ")";
            graphics.Clear(Color.Empty);
            graphics.DrawString(message, font, defaultBrush, position);

            _ammo.Update(_graphics, ammo);
            _health.Update(_graphics, health);
            foreach (LogString log in _logs.ToArray())
            {
                graphics.DrawString(log.GetMessage(), _logFont, defaultBrush, log.GetPosition());
                log.Move(-2.2f);
            }
            if (logCount > 0)
                logCount--;
            if (health < 0 && !dead)
            {
                dead = true;
                Log("[Info] You just died! Restart the game to retry.");
            }
        }

        public override void Destroy()
        {
            base.Destroy();
            graphics.Dispose();
        }


        /// <summary>
        /// Logs a message to the screen
        /// </summary>
        /// <param name="message"></param>
        public void Log(string message)
        {
            if (logCount == 0)
            {
                LogString log = new LogString(message, new PointF(20, 580));
                _logs.Add(log);
                logCount = 7;
            }
        }

        private class LogString
        {
            private string _message;
            private PointF _position;

            public LogString(string message, PointF position)
            {
                _message = message;
                _position = position;
            }

            public string GetMessage()
            {
                return _message;
            }

            public PointF GetPosition()
            {
                return _position;
            }

            public void Move(float y)
            {
                _position.Y += y;
            }
        }
    }
}